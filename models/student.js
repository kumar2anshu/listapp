module.exports = (sequelize, type) => {
    return sequelize.define('student', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: type.STRING,
            validate: {
                notEmpty: true,
                isAlpha: true,
                len: {
                    args: [4, 50],
                    msg: "Name  must be between 4 and 50 characters in length"
                },

            }



        }
    })
}