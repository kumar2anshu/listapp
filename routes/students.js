const express = require('express');
const bodyParser = require('body-parser')
const app = express()
app.use(bodyParser.json())
var validator = require('validator');
// validator.isEmail('foo@bar.com'); //=> true


// var app = express.Router();
const { Student } = require('../config/seqelize')




app.get('/addstudent', function (req, res, next) {
    res.render('students/addstudent')
})



// Adding a  new student
app.post('/addstudent', async (req, res) => {
    // console.log(req.body.studentname)
    try {
        const student = await Student.create({ name: req.body.studentname });
        console.log("student name added successfully");
        res.redirect('/students')

    } catch (err) {
        console.log(err.errors[0].message)
        res.json(err.errors[0].message)
    }

    //this is old method to handle promise

    // Student.create({ name: req.body.studentname }).then(student => {
    //     // console.log(student);
    //     res.redirect('/students');

    // })
})
// get or view all students
app.get('/', (req, res) => {

    res.render("students/home");

})
app.get("/viewstudent", (req, res) => {
    Student.findAll().then(allStudents => {
        console.log(allStudents)
        res.render("students/viewstudent", { allStudents });
    })

})
// console.log(all);

module.exports = app;
