const Sequelize = require('sequelize')
const StudentModel = require('../models/student')

const sequelize = new Sequelize('library', 'sigma', 'Sigma@123', {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
})

const Student = StudentModel(sequelize, Sequelize)
sequelize.sync({ force: false })
    .then(() => {
        console.log(`Database & tables succesfully created!`)
    })

module.exports = {
    Student
}